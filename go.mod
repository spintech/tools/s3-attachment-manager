module gitlab.com/spintech/tools/s3-attachment-manager

go 1.14

require (
	github.com/minio/minio-go/v7 v7.0.10
	github.com/stretchr/testify v1.4.0
)
