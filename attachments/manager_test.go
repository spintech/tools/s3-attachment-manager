package attachments_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/spintech/tools/s3-attachment-manager/attachments"
)

var (
	config  attachments.Config
	manager attachments.AttachmentManager
)

const (
	folderName = "dummy_folder_name"
	fileName   = "dummy.jpg"
)

func TestMain(m *testing.M) {
	rawBytes, err := ioutil.ReadFile("./../config.json")
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(rawBytes, &config)
	if err != nil {
		log.Fatal(err)
	}

	manager, _ = attachments.NewManager(&config)

	os.Exit(m.Run())
}

func TestNewManager(t *testing.T) {
	_, err := attachments.NewManager(&config)
	require.NoError(t, err)
}

func TestManager_GetFile(t *testing.T) {
	file, info, rawBytes := readDummyFile()
	filePath := path.Join(folderName, fileName)

	// put file
	err := manager.PutFile(filePath, file, info.Size())
	require.NoError(t, err)

	t.Run("should return a file", func(t *testing.T) {
		// get file
		reader, err := manager.GetFile(filePath)
		require.NoError(t, err)

		buf := bytes.Buffer{}
		_, _ = buf.ReadFrom(reader)
		assert.Equal(t, rawBytes, buf.Bytes())
	})

	t.Run("should return an error when file is not exist", func(t *testing.T) {
		// remove file
		require.NoError(t, manager.RemoveFile(filePath))

		// try to get a file
		_, err = manager.GetFile(filePath)
		require.Error(t, err, "no such file")
	})
}

func TestManager_RemoveFiles(t *testing.T) {
	t.Run("should remove files on 1st level of nesting", func(t *testing.T) {
		putFiles(t, folderName)
		require.NoError(t, manager.RemoveFiles(folderName))

		_, err := manager.GetFile(path.Join(folderName, "1_file.jpg"))
		assert.Error(t, err, "no such file")
	})

	t.Run("should remove files on 2 level of nesting", func(t *testing.T) {
		folderPath := path.Join("alanna_abernathy_fffe3b", "149ccc")

		putFiles(t, folderPath)
		require.NoError(t, manager.RemoveFiles(folderPath))

		_, err := manager.GetFile(path.Join(folderPath, "1_file.jpg"))
		assert.Error(t, err, "no such file")
	})

	t.Run("should remove only provided files if any", func(t *testing.T) {
		putFiles(t, folderName)
		require.NoError(t, manager.RemoveFiles(folderName, "1_file.jpg", path.Join(folderName, "2_file.jpg")))

		_, err := manager.GetFile(path.Join(folderName, "0_file.jpg"))
		assert.NoError(t, err, "no such file")

		_, err = manager.GetFile(path.Join(folderName, "2_file.jpg"))
		assert.Error(t, err, "no such file")

		// cleanup
		require.NoError(t, manager.RemoveFiles(folderName))
	})

	t.Run("should not allow to remove a root directory", func(t *testing.T) {
		putFiles(t, folderName)

		require.Error(t, manager.RemoveFiles(""), attachments.NotAllowedErrMsg)
		require.Error(t, manager.RemoveFiles("/"))

		_, err := manager.GetFile(path.Join(folderName, "0_file.jpg"))
		assert.NoError(t, err, "no such file")

		// cleanup
		require.NoError(t, manager.RemoveFiles(folderName))
	})

}

func readDummyFile() (io.Reader, os.FileInfo, []byte) {
	filePath := path.Join("./testdata/", fileName)
	rawBytes, _ := ioutil.ReadFile(filePath)

	info, _ := os.Stat(filePath)

	return bytes.NewReader(rawBytes), info, rawBytes
}

func putFiles(t *testing.T, folderPath string) {
	_, info, rawBytes := readDummyFile()

	for i := 0; i < 3; i++ {
		filePath := path.Join(folderPath, fmt.Sprintf("%d_file.jpg", i))
		file := bytes.NewReader(rawBytes)
		require.NoError(t, manager.PutFile(filePath, file, info.Size()))
	}

}
