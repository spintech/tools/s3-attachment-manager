package attachments

import (
	"context"
	"errors"
	"io"
	"log"
	"strings"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

type Manager struct {
	client *minio.Client
	bucket string
}

const NotAllowedErrMsg = "you shall not pass"

func NewManager(cfg *Config) (*Manager, error) {
	minioClient, err := minio.New(cfg.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(cfg.Key, cfg.Secret, ""),
		Secure: cfg.UseSSL,
	})

	if err != nil {
		return nil, err
	}

	return &Manager{client: minioClient, bucket: cfg.Bucket}, nil
}

func (m *Manager) GetFile(objName string) (io.Reader, error) {
	obj, err := m.client.GetObject(context.Background(), m.bucket, objName, minio.GetObjectOptions{})
	if err != nil {
		return nil, err
	}
	stats, _ := obj.Stat()
	if stats.Key == "" && stats.Size == 0 {
		return nil, errors.New("no such file")
	}
	return obj, nil
}

func (m *Manager) PutFile(objName string, file io.Reader, size int64, contentType ...string) error {
	contentT := "application/octet-stream"

	if len(contentType) > 0 {
		contentT = contentType[0]
	}

	info, err := m.client.PutObject(context.Background(), m.bucket, objName, file, size, minio.PutObjectOptions{ContentType: contentT})
	if err != nil {
		return err
	}
	log.Printf("Successfully uploaded file: %s bytes: %d\n", objName, info.Size)
	return nil
}

func (m *Manager) RemoveFile(objName string) error {
	if objName == "" || objName == "/" {
		return errors.New(NotAllowedErrMsg)
	}

	return m.client.RemoveObject(context.Background(), m.bucket, objName, minio.RemoveObjectOptions{})
}

// RemoveFiles - allows to remove files from folder
// folderPath - is a virtual folder
// Note: in case no files specified all files in folder will be removed
func (m *Manager) RemoveFiles(folderPath string, files ...string) error {
	if folderPath == "" || folderPath == "/" {
		return errors.New(NotAllowedErrMsg)
	}

	objectsCh := make(chan minio.ObjectInfo)

	go func() {
		defer close(objectsCh)

		for object := range m.client.ListObjects(context.Background(), m.bucket, minio.ListObjectsOptions{
			Prefix:    folderPath,
			Recursive: true,
		}) {
			if object.Err != nil {
				log.Println(object.Err)
				return
			}

			if len(files) == 0 {
				objectsCh <- object
				continue
			}

			for _, path := range files {
				if strings.HasSuffix(object.Key, path) {
					objectsCh <- object
					break
				}
			}
		}

	}()

	for rErr := range m.client.RemoveObjects(context.Background(), m.bucket, objectsCh, minio.RemoveObjectsOptions{}) {
		return rErr.Err
	}

	return nil
}
