package attachments

import "io"

type Getter interface {
	GetFile(objName string) (io.Reader, error)
}

type Uploader interface {
	PutFile(objName string, file io.Reader, size int64, contentType ...string) error
}

type Remover interface {
	RemoveFile(objName string) error
	RemoveFiles(folderPath string, files ...string) error
}

type AttachmentManager interface {
	Getter
	Uploader
	Remover
}
