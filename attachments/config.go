package attachments

type Config struct {
	Endpoint string
	UseSSL   bool
	Key      string
	Secret   string
	Bucket   string
}
